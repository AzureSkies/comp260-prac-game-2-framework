﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class Goal : MonoBehaviour {

	public Transform startingPos;
	private Rigidbody rigidbody;
	public delegate void ScoreGoalHandler(int player);
	public ScoreGoalHandler scoreGoalEvent;
	public int player;


	void Start () {
		//audio = GetComponent<AudioSource> ();
		rigidbody = GetComponent<Rigidbody>();
		ResetPosition();
	}

	public void ResetPosition() {
		// teleport to the starting position
		rigidbody.MovePosition(startingPos.position);
	}

	void OnTriggerEnter(Collider collider) {
		// play score sound
		//GetComponent<AudioSource>().PlayOneShot(scoreClip);

		// reset the puck to its starting position
		PuckControl puck = collider.gameObject.GetComponent<PuckControl>();
		puck.ResetPosition();

		if (scoreGoalEvent != null) {
			scoreGoalEvent(player);
		}

	}

}
