﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;




[RequireComponent(typeof(Rigidbody))]
public class MovePaddleKeysd : MonoBehaviour {

	// Use this for initialization
	public float maxSpeed = 5.0f;
	public Rigidbody bodyRigid;
	public Rigidbody velocity;


	void Start(){
		bodyRigid = GetComponent<Rigidbody>();
	}

	void Update()
	{
		// get the input values
		Vector2 direction;
		direction.x = Input.GetAxis("Horizontal");
		direction.y = Input.GetAxis("Vertical");

		bodyRigid.velocity = maxSpeed * direction * Time.deltaTime * 80f;
	}
}