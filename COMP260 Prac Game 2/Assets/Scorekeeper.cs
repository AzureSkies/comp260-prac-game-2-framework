﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Scorekeeper : MonoBehaviour {

	public int scorePerGoal = 1;
	private int[] score = new int[2];
	public Text[] scoreText;


	void Start () {    
		// subscribe to events from all the Goals
		Goal[] goals = FindObjectsOfType<Goal> ();
		 
		for (int i = 0; i < goals.Length; i++) {
			goals[i].scoreGoalEvent += OnScoreGoal;
		}

		// reset the scores to zero
		for (int i = 0; i < score.Length; i++) {
			score [i] = 0;
			scoreText [i].text = "0";
		}

	}
	 
	public void OnScoreGoal(int player) {
		// add points to the player whose goal it is
		score[player] += scorePerGoal;
		scoreText[player].text = score[player].ToString ();
		Debug.Log ("Player " + player + ": " + score[player]);
		winCondition();
	}
		
	public void winCondition(){
		if (score [0] >= 10) {
			scoreText [2].text = "Player 2 wins!";
			Time.timeScale = 0;
		}

		else if (score [1] >= 10) {
			scoreText [2].text = "Player 1 wins!";
			Time.timeScale = 0;
		}


	}
	 
}
