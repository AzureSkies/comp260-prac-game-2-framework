﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class AIMovement : MonoBehaviour {


	private Rigidbody rigidbody;



	// Use this for initialization
	void Start () {
		rigidbody = GetComponent<Rigidbody>();
		rigidbody.useGravity = false;


	}
	
	// Update is called once per frame
	void Update () {
		// Get vector between goal center and puck, then find midpoint of vector.
		// (vector1 + vector2)/2

		Vector3 blueGoalCenter = GameObject.Find ("Blue goal trigger").transform.position;
		Vector3 puck = GameObject.Find ("Puck").transform.position;
		Vector3 redGoalCenter = GameObject.Find ("Red goal trigger").transform.position;


		Vector3 defense = (blueGoalCenter + puck) / 2;
		Vector3 attack = (redGoalCenter + puck) / 2;

		/*if (attack.magnitude > defense.magnitude) {
			rigidbody.MovePosition ((defense + attack)/2);
		} else {*/
			rigidbody.velocity = (defense - transform.position);
		//}



	}



}
